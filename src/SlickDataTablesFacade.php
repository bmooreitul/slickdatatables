<?php

namespace Itul\SlickDataTables;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Itul\SlickDataTables\Skeleton\SkeletonClass
 */
class SlickDataTablesFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'slick-data-tables';
    }
}
