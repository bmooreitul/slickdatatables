<?php

namespace Itul\SlickDataTables;
use Itul\SqlToLaravel;

class SlickDataTables extends \Yajra\Datatables\Datatables{

    public static function of($query){

        if(is_string($query)) $query = \Itul\SqlToLaravel\SqlToLaravel::convert($query);

        if(is_array(request()->_ranges) && !empty(request()->_ranges)){
            foreach(request()->_ranges as $colKey => $rangeData){
                if(isset($rangeData['start']) && strlen($rangeData['start'])){
                    if($rangeData['type'] == 'numeric'){
                        $query->where($rangeData['search']['name'], '>=', $rangeData['start']);
                    }  
                    elseif($rangeData['type'] == 'date'){
                        $query->where($rangeData['search']['name'], '>=', \Carbon\Carbon::parse($rangeData['start']));
                    }                   
                }
                if(isset($rangeData['end']) && strlen($rangeData['end'])){
                    if($rangeData['type'] == 'numeric'){
                        $query->where($rangeData['search']['name'], '<=', $rangeData['end']);
                    }
                    elseif($rangeData['type'] == 'date'){
                        $query->where($rangeData['search']['name'], '<=', \Carbon\Carbon::parse($rangeData['end']));
                    }  
                }
            }
        }

        return parent::of($query);
    }

    /*
    public function filter($callback){

        $callback2 = function($instance){
            if(is_array(request()->_ranges) && !empty(request()->_ranges)){
                foreach(request()->_ranges as $colKey => $rangeData){
                    if(isset($rangeData['start']) && strlen($rangeData['start'])){
                        if($rangeData['type'] == 'numeric'){
                            $instance->where($rangeData['search']['name'], '>=', $rangeData['start']);
                        }  
                        elseif($rangeData['type'] == 'date'){
                            $instance->where($rangeData['search']['name'], '>=', \Carbon\Carbon::parse($rangeData['start']));
                        }                   
                    }
                    if(isset($rangeData['end']) && strlen($rangeData['end'])){
                        if($rangeData['type'] == 'numeric'){
                            $instance->where($rangeData['search']['name'], '<=', $rangeData['end']);
                        }
                        elseif($rangeData['type'] == 'date'){
                            $instance->where($rangeData['search']['name'], '<=', \Carbon\Carbon::parse($rangeData['end']));
                        }  
                    }
                }
            }
        };

        $callbacks = [$callback2, $callback];

        $finalCallback = function($instance) use($callbacks){
            foreach($callbacks as $c) $c($instance);
        };

        parent::filter(function($instance) use($finalCallback){
            finalCallback($instance);
        });

    }
    */
}
