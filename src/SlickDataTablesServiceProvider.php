<?php

namespace Itul\SlickDataTables;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class SlickDataTablesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'slick-data-tables');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'slick-data-tables');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');



        if ($this->app->runningInConsole()) {

            if(!file_exists(config_path('slick-data-tables.php'))){
                $this->publishes([
                    __DIR__.'/../config/config.php' => config_path('slick-data-tables.php'),
                ], 'laravel-assets');
            }
            

            // Publishing the views.
            /*$this->publishes([
                __DIR__.'/../resources/views' => resource_path('views/vendor/slick-data-tables'),
            ], 'views');*/

            // Publishing assets.
            /*$this->publishes([
                __DIR__.'/../resources/assets' => public_path('vendor/slick-data-tables'),
            ], 'assets');*/

            // Publishing the translation files.
            /*$this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/vendor/slick-data-tables'),
            ], 'lang');*/

            // Registering package commands.
            // $this->commands([]);
        }

        Blade::directive('sdtStyles', function($expression){
            return '<?php echo \'<link rel="stylesheet" href="//unpkg.com/vanilla-datatables@latest/dist/vanilla-dataTables.min.css">\';
                if(config(\'slick-data-tables.bootstrap\') != false){
                    echo \'<link rel="stylesheet" href="//cdn.datatables.net/1.12.1/css/dataTables.\'.config(\'slick-data-tables.bootstrap\').\'.min.css">\';
                }
            ?>';
        });

        Blade::directive('sdtScripts', function($expression){
            return '<?php 
                echo \'<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.2/moment.min.js"></script>\';
                echo \'<script type="text/javascript" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>\';
                echo \'<script type="text/javascript" src="//cdn.datatables.net/datetime/1.1.2/js/dataTables.dateTime.min.js"></script>\';
                if(config(\'slick-data-tables.bootstrap\') != false){
                    echo \'<script type="text/javascript" src="//cdn.datatables.net/1.12.1/js/dataTables.\'.config(\'slick-data-tables.bootstrap\').\'.min.js"></script>\';
                }
                echo \'<script type="text/javascript" src="//cdn.jsdelivr.net/gh/bmooreitul/slickFilters@\'.config(\'slick-data-tables.slickFilters\').\'/slickFilters.min.js"></script>\';    
            ?>';
        });
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'slick-data-tables');

        // Register the main class to use with the facade
        $this->app->singleton('slick-data-tables', function () {
            return new SlickDataTables;
        });
    }
}
